/**传入Flysel的遍历方法的回调函数 */
interface ergodicCallBlck {
    (flysel: Flysel, index: number, srcFlysel: ThisType<Flysel>): any | Promise<any>;
}
/**元素事件选项 */
interface ListenerOption {
    /**是否在事件捕获阶段触发 */
    capture?: boolean;
    /**在添加之后最多只调用一次 */
    once?: boolean;
    /**禁用preventDefault()阻止默认行为 */
    passive?: boolean;
    /**事件中止信号 */
    signal?: AbortController;
    /**委托选择器 */
    entrust?: string;
}
/**元素事件映射 */
interface ElementEventMap extends HTMLElementEventMap {
    /**鼠标持续按下并滑动 */
    mousepressmove: MouseEvent;
    stylechange: StyleEvent;
}
/**类数组对象 */
interface ClassArray {
    length: number;
    [index: number]: any;
}
/**对象索引 */
interface ObjectIndex<V = any> {
    [k: string]: V;
}
/**表单收集类选项 */
interface FormJsonOption {
    /**输入元素选择器 */
    input?: string;
    /**复数元素选择器 */
    plural?: string;
    /**默认/缺省数据 */
    defaultValue?: {
        [x: string]: any;
    };
}
declare class StyleEvent extends Event {
    style: string;
    value: string;
    constructor(type?: string, option?: any);
}
export declare class FlyselError extends Error {
}
/**form表单收集 */
export declare class FormJson {
    #private;
    [x: string]: any;
    constructor(form: Flysel, option?: FormJsonOption);
    set(name: string, value: any, element?: HTMLElement): any;
    collect(): void;
    getOption(): {
        input: string;
        plural: string;
        defaultValue: {};
    } & FormJsonOption;
}
/**事件侦听器函数 */
interface EventListener<T extends keyof ElementEventMap = any> {
    (this: Flysel, event: ElementEventMap[T]): any;
}
/**Flysel事件处理类选项映射 */
interface FlyselEventOptions {
    once: boolean;
}
/**Flysel事件处理类事件映射 */
interface FlyselEventMap {
}
/**Flysel事件系统 */
export declare class FlyselEvent<H extends FlyselEvent<H, M>, M extends FlyselEventMap> {
    #private;
    onaddlistener: ((this: H, event: {
        type: string;
        listener: (this: any, event: any) => void;
    }) => void) | null;
    onremovelistener: ((this: H, event: {
        type: string;
        listener: (this: any, event: any) => void;
        hasDelete: boolean;
    }) => void) | null;
    onemitevent: ((this: H, event: {
        type: string;
        length: number;
    }) => void) | null;
    /**
     * 侦听事件
     * @param type 事件类型
     * @param listener 事件侦听器
     * @param option 事件选项
     * @returns
     */
    on<T extends keyof M>(type: T, listener: (this: H, event: M[T]) => void, option?: FlyselEventOptions): boolean;
    /**
     * 取消事件侦听器
     * @param type 事件类型
     * @param listener 事件侦听器
     * @param option 事件选项
     * @returns
     */
    off<T extends keyof M>(type: T, listener: (this: H, event: M[T]) => void): boolean;
    /**
     * 发生事件
     * @param type 事件类型
     * @param event 事件参数
     * @returns
     */
    emit<T extends keyof M>(type: T, event: M[T]): boolean;
}
/**钩子事件 */
interface HookEventMap extends FlyselEventMap {
    on: {
        flysel: Flysel;
        type: keyof ElementEventMap;
        listener: EventListener;
        option?: ListenerOption;
    };
    off: {
        flysel: Flysel;
        type: keyof ElementEventMap;
        listener: EventListener;
    };
    instantiation: {
        flysel: Flysel;
        element: HTMLElement | any;
    };
}
/**内置事件（钩子函数） */
export declare const hook: FlyselEvent<FlyselEvent<any, HookEventMap>, HookEventMap>;
/**
 * Flying Select 类
 */
export declare class Flysel<E extends HTMLElement = HTMLElement> {
    [index: number]: E;
    [key: string]: any;
    splice(): void;
    /**长度 */
    length: number;
    constructor(element?: E);
    /**返回第一个原生E对象 */
    get e(): E;
    /**
     * 获取指定索引的Flysel对象
     * @param {number} index 索引
     * @returns {Flysel}
     */
    get(index: number): Flysel;
    /**
     * 添加元素
     * @param element
     * @returns
     */
    add(element: E): this;
    /**
     * 过滤元素
     * @param {ergodicCallBlck} callBlck 回调函数
     */
    filter(callBlck: ergodicCallBlck): Flysel<HTMLElement> | Promise<Flysel<HTMLElement>>;
    /**筛选 */
    screen(selector: string): Flysel<HTMLElement>;
    /**
     * 遍历元素
     * @param {ergodicCallBlck} callBlck 回调函数
     */
    forEach(callBlck: ergodicCallBlck): Promise<void> | undefined;
    /**
     * 遍历元素并返回一个新数组
     * @param {ergodicCallBlck} callBlck 回调函数
     */
    map(callBlck: ergodicCallBlck): any[] | Promise<any[]>;
    /**
     * 返回是否存在指定element
     * @param element
     * @returns
     */
    includes(element: any): boolean;
    /**
     * 转为字符串
     * @param element
     * @returns
     */
    toString(element: E): any;
    /**
     * 获取相对窗口的位置
     * @returns
     */
    offwin(): {
        top: number;
        left: number;
    };
    /**
     *
     * @returns 获取相对html顶部的位置
     */
    offset(): {
        top: number;
        left: number;
    };
    /**收集表单 */
    collect(option?: FormJsonOption): FormJson;
    /**选择方法 */
    /**
     * 从后代中选择
     * @param {string} selector
     * @returns
     */
    find<E extends HTMLElement = HTMLElement>(selector?: string): Flysel<E>;
    /**
     * 从子代中选择
     * @param {string} selector
     * @returns
     */
    child<E extends HTMLElement = HTMLElement>(selector?: string): Flysel<E>;
    /**
     * 从前辈元素中选择
     * @param {string} selector
     */
    parent<E extends HTMLElement = HTMLElement>(selector: string): Flysel<E>;
    /**
     * 从兄弟元素中选择
     * @param {string} selector
     * @returns
     */
    sibling<E extends HTMLElement = HTMLElement>(selector?: string): Flysel<E>;
    /**操作类名 */
    /**
     * 添加类名
     * @param {string[]} className
     * @returns
     */
    addClass(...className: string[]): this;
    /**
     * 删除类名
     * @param {string[]} className
     * @returns
     */
    removeClass(...className: string[]): this;
    /**
     * boolean为true则添加类名反之则删除类名
     * @param {Boolean} boolean
     * @param {string[]} className
     * @returns
     */
    booleClass(boolean: Boolean, ...className: string[]): this;
    /**
     * 检测是否存在指定类名
     * @param {string} className
     * @returns
     */
    hasClass(className: string): boolean;
    /**
     * 若存在已类名则删除类名，反之则添加类名
     * @param {string} className
     * @returns
     */
    toggleClass(...className: string[]): this;
    /**
     * 活跃类名：添加指定类名并删除所有兄弟元素的指定类名
     * @param {string[]} className
     * @returns
     */
    activeClass(...className: string[]): this;
    /**操作事件 */
    /**
     * 添加事件侦听器
     * @param {string} type
     * @param {FlyselListener} listener
     * @returns
     */
    on<T extends keyof ElementEventMap>(listenerObject: {
        [type in T]: EventListener<T>;
    }, option?: ListenerOption): this;
    on<T extends keyof ElementEventMap>(type: T, listener: EventListener<T>, option?: ListenerOption): this;
    /**
     * 移除事件侦听器
     * @param {string} type
     * @param {FlyselListener} listener
     * @returns
     */
    off<T extends keyof ElementEventMap>(type: T, listener: EventListener<T>): this;
    /**常用方法代理 */
    /**
     * 设置css
     * @param style style对象
     */
    css(style: string): string;
    /**
     * 设置css
     * @param style style对象
     */
    css(style: CSSStyleDeclaration): this;
    /**
     * 设置css
     * @param attr css属性名
     * @param value css属性值
     */
    css<T extends keyof CSSStyleDeclaration>(attr: T, value: CSSStyleDeclaration[T]): this;
    upIncludes(selector: string): boolean;
    /**
     * 在元素内添加元素节点或文本
     * @param {...(string | Element)[]} eles
     * @returns
     */
    append(...eles: (string | Element)[]): this;
    /**
     * 在元素内添加HTML
     * @param html
     * @returns
     */
    addHTML(html: string): this;
    /**
     * 在元素内添加节点
     * @param element
     * @returns
     */
    addElement(element: Element): this;
    /**
     * 插入HTML
     * @param where 插入位置
     * @param html
     * @returns
     */
    insertHTML(where: InsertPosition, html: string): this;
    /**
     * 插入Element
     * @param where 插入位置
     * @param Element
     * @returns
     */
    insertElement(where: InsertPosition, element: Element): this;
    /**
     * 返回一个包含所有元素的attr属性的数组（对象属性）
     * @param {string} attr
     */
    prop(attr: string): any;
    /**
     * 将所有元素的attr属性设为value（对象属性）
     * @param attr
     * @param value
     */
    prop(attr: string, value: any): this;
    /**
     * 返回一个包含所有元素的attr属性的数组（内联属性）
     * @param {string} attr
     */
    attr(attr: string): any;
    /**
     * 将所有元素的attr属性设为value（内联属性）
     * @param attr
     * @param value
     */
    attr(attr: string, value: any): this;
    /**
     * 获取对象属性为数组
     * @param {string} prop
     * @returns
     */
    getProp(prop: string): any[];
    /**
     * 获取内联属性为数组
     * @param {string} attr
     * @returns
     */
    getAttr(attr: string): (string | null)[];
    /**
     * 模板渲染
     * @param data
     * @param option
     */
    template(data: any, option: {
        preset?: boolean;
    }): void;
    /**
     * 滑动相对距离
     * @param x
     * @param y
     */
    scrollBy(x?: number, y?: number): this;
    /**
     * 滑动相对距离
     * @param options
     */
    scrollBy(options?: ScrollToOptions): this;
    /**
     * 滑动至指定距离
     * @param x
     * @param y
     */
    scrollTo(x?: number, y?: number): this;
    /**
     * 滑动至指定距离
     * @param options
     */
    scrollTo(options?: ScrollToOptions): this;
    /**触发click */
    click(): this;
    /**触发focus */
    focus(): this;
    /**触发blur */
    blur(): this;
    /**隐藏元素(display:none) */
    hide(): this;
    /**显示元素(display:block) */
    block(): this;
    /**显示元素(display:flex) */
    flex(): this;
    /**常用属性代理 */
    /**文本 */
    get text(): string;
    set text(v: string);
    /**HTML */
    get html(): string;
    set html(v: string);
    /**元素宽度，等同于offsetWidth */
    get width(): number;
    set width(v: number);
    /**元素宽度，等同于offsetHeight */
    get height(): number;
    set height(v: number);
    /**name */
    get name(): any;
    set name(v: any);
    /**id */
    get id(): string;
    set id(v: string);
    /**value */
    get value(): any;
    set value(v: any);
    /**type */
    get type(): any;
    set type(v: any);
    /**checked */
    get checked(): any;
    set checked(v: any);
    /**checked */
    get src(): any;
    set src(v: any);
    /**left */
    get left(): number;
    set left(v: number);
    /**top */
    get top(): number;
    set top(v: number);
    /**只读属性 */
    get clientWidth(): number;
    get clientHeight(): number;
    get scrollWidth(): number;
    get scrollHeight(): number;
    get scrollLeft(): number;
    get scrollTop(): number;
    /**静态方法 */
    /**
     * 从类数组对象生成实例
     * @param {ClassArray} array 类数组对象
     * @returns
     */
    static from<E extends HTMLElement = HTMLElement>(array: ClassArray): Flysel<E>;
}
/**
 * flying select
 * @param selector
 * @returns
 */
export declare const flysel: {
    (selector?: string | HTMLElement | ClassArray, context?: HTMLElement | ClassArray | Document): Flysel<HTMLElement>;
    document: Flysel<HTMLElement>;
    body: Flysel<HTMLElement>;
    html: Flysel<HTMLElement>;
    on: {
        <T extends keyof ElementEventMap>(listenerObject: { [type in T]: EventListener<T>; }, option?: ListenerOption | undefined): this;
        <T_1 extends keyof ElementEventMap>(type: T_1, listener: EventListener<T_1>, option?: ListenerOption | undefined): this;
    };
    off: <T_2 extends keyof ElementEventMap>(type: T_2, listener: EventListener<T_2>) => Flysel<any>;
};
export default flysel;
/**
 * 解析模板
 * @param {string} tem
 * @param {*} option
 */
export declare function template(tem: string, option?: {
    preset?: boolean;
}): (...args: ((index: number, ...int: string[]) => string)[]) => string;
/**插件参数 */
export interface SyncPlugin extends ObjectIndex {
    /**同步对象 */
    object: Object[];
    /**同步表单 */
    form: {
        el: Flysel | Flysel | string;
        option: FormJsonOption;
    } | string;
    /**同步样式 */
    style: Flysel;
    /**更新事件 */
    change: (event: {
        key: string;
        value: any;
        item?: Value;
        add?: boolean;
    }) => void;
}
/**同步选项 */
declare type SyncOption = SyncPlugin | ObjectIndex;
/**支持同步的数据类型 */
declare type Value = string | number | undefined;
/**Sync选项映射 */
interface SyncFlyselEventMap extends FlyselEventMap {
    change: {
        key: string;
        value: Value | Set<Value>;
        item?: Value;
        add?: boolean;
    };
    set: {
        key: string;
        value: Value | Set<Value>;
    };
    item: {
        key: string;
        item: Value;
        add?: boolean;
    };
    add: {
        key: string;
        item: any;
    };
    del: {
        key: string;
        item: any;
    };
}
/**数据同步 */
export declare class Sync<S extends ObjectIndex = any> extends FlyselEvent<Sync, SyncFlyselEventMap> {
    src: S;
    proxy: S;
    constructor(src: S, option?: SyncOption);
    set(key: string, value: Value | Set<Value>): boolean;
    item(key: string, item: Value, add?: boolean): void;
    add(key: string, item: Value): void;
    del(key: string, item: Value): void;
    toJSON(): any;
}
/**Set同步 */
export declare class SetSync {
    #private;
    constructor(src: Set<unknown> | undefined, sync: Sync, key: string);
    get size(): number;
    has(v: any): boolean;
    clear(): void;
    add(value: any): this;
    delete(value: any): boolean;
    entries(): IterableIterator<[unknown, unknown]>;
    forEach(callBack: (value: any, value2: any, set: Set<any>) => void): void;
    del(value: any): boolean;
}
/**
 * 数据同步
 * @param option 配置
 * @returns
 */
export declare function sync<S>(src: S, option?: SyncOption): S;
