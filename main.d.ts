interface Mode {
    /**键盘按键按下 */
    keydown: KeyboardEvent;
    /**键盘按键抬起 */
    keyup: KeyboardEvent;
    /**鼠标按键按下 */
    mousedown: MouseEvent;
    /**鼠标按键抬起 */
    mouseup: MouseEvent;
    /**鼠标按键移动 */
    mousemove: MouseEvent;
    /**鼠标中键滚动 */
    wheel: WheelEvent;
}
declare type ConstraintMode = 'key' | 'mouse' | 'wheel';
interface ConditionFunction {
    (event: Event, ...age: any[]): any;
}
interface Condition {
    [k: string]: boolean | ConditionFunction;
}
interface Listener {
    (event: Event): any;
}
interface InitKey {
    word: string;
    key?: string[];
}
interface InitKeySet {
    [x: string]: InitKey;
}
interface CommandOption {
    key?: string;
    constraintMode?: ConstraintMode;
    preventDefault?: boolean;
    disableModify?: boolean;
}
declare type DefaultBehavior = 'menu' | 'wheel' | 'Tab' | 'F*' | 'Ctrl+*' | 'Shift+*' | 'Alt+*' | 'Ctrl+Shift+*' | 'Ctrl+Alt+*' | 'Shift+Alt+*';
declare const erreoMap: {
    '001': string;
};
declare function CommandError(code: string, ...ages: any[]): any;
declare const preventDefaultReg: Set<RegExp>;
declare function defaultBehavior(event: Event, keyCode: string): void;
/**
 * 阻止浏览器默认行为
 * @param {DefaultBehavior | string} ages 描述阻止按键的字符串列表，可多填
 */
declare function preventDefault(...ages: (DefaultBehavior | string | RegExp)[]): void;
/**事件载体 */
declare let carrier: Document | HTMLElement;
/**快捷键字符串: Command对象集合*/
declare const commandMap: Map<string, Set<Command>>;
/**command字符串: Command对象*/
declare const commandSet: Map<string, Command>;
/**功能键映射 */
declare const FUNCTION_KEY_MAP: {
    [k: string]: string;
};
/**屏蔽键列表 */
declare const SHIELD_KEY_LIST: string[];
declare const MOUSEPRESS: string[];
declare const SEPARATOR = "#.[]:";
declare const KEYMAP: {
    wordToCodes: Map<any, any>;
    codeToWords: Map<any, any>;
    keyToCodes: Map<any, any>;
    length: number;
    /**
     * 单词转键值码
     * @param {string} word
     * @returns
     */
    wordToCode(word: string): any;
    /**
     * 键值码转单词
     * @param {string} code
     * @returns
     */
    codeToWord(code: string): any;
    /**
     * event.code转键值码
     * @param {string} key
     * @returns
     */
    keyToCode(key: string): any;
    /**
     * event.code转单词
     * @param {string} key
     * @returns
     */
    keyToWord(key: string): any;
    /**
     * 添加键映射
     * @param {string} word
     * @param {string[]} key
     * @param {string} code
     * @returns {string} 键值码
     */
    addCode({ word, key }: {
        word: string;
        key?: string[] | undefined;
    }, code?: string | undefined): string;
};
/**条件 */
declare const condition: Condition;
/**模式约束对象 */
declare const ConstraintModeMap: {
    key: {
        mode: string[];
    };
    mouse: {
        mode: string[];
        key: RegExp;
    };
    wheel: {
        mode: string[];
        key: RegExp;
    };
};
/**addEventListener事件默认选项 */
declare const listenerOption: {
    capture: boolean;
    passive: boolean;
};
declare const findKeyWordCache: Map<string, any>;
declare const customKeyCode: {
    code: number;
    initKey: InitKey;
}[];
/**配置对象 */
declare const config: {
    /**快捷键字符串分隔符 */
    shortcutKeySeparator: string;
    /**条件字符串分隔符 */
    conditionSeparator: string;
    /**迭代查找的顶层元素 */
    topElement: Document;
    /**
     * 导入自定义键值码
     * @param {string[]} data 配置项
     */
    importCustomKeyCode(data: {
        code: number;
        initKey: InitKey;
    }[]): void;
    exportCustomKeyCode(): {
        code: number;
        initKey: InitKey;
    }[];
    customKeyCode(code: number, initKey: InitKey): void;
    /**Command默认缺省配置 */
    readonly CommandDefaultOption: {
        key: string;
        constraintMode: string;
        preventDefault: boolean;
        disableModify: boolean;
    };
};
declare function callBack(d: any): void;
/**
 * 迭代查找元素
 * @param {string} findKeyWord 查找关键词
 * @param {HTMLElement} element 源元素
 * @returns {HTMLElement | undefined} 返回查到到的第一个元素
 */
declare function findElement(findKeyWord: string, element: any): HTMLElement | undefined;
/**
 * 解析查询字符串
 * @param {string} keyWord 查询字符串
 * @param {string} separator 分隔符
 * @param {Function} callBack 回调函数
 * @returns {Object[]} 解析结果
 */
declare function analyseKeyWord(keyWord: string, separator: string, callBack: Function): {
    sep: string | undefined;
    word: any;
}[];
/**
 * 匹配解析结果
 * @param {Object[]} data 解析结果
 * @param {HTMLElement} element 元素
 * @returns {boolean}
 */
declare function matchedElement(data: any, element: HTMLElement): Boolean | undefined;
/**
 * 获取快捷键字符串
 * @param {KeyboardEvent} event
 * @returns {string}
 */
declare function getKeyCode(event: Event, key: string): string;
/**
 * 获取命令
 * @param {string} cmd 命令名
 * @returns {Command|undefined} 命令
 */
declare function getCommand(cmd: string): Command | undefined;
/**
 * 搜索按键命令
 * @param {string} key 按键字符串
 * @returns {Command[]} 命令数组（只读）
 */
declare function searchKey(key: string): Command[];
/**Command默认缺省配置 */
declare const CommandDefaultOption: {
    key: string;
    constraintMode: string;
    preventDefault: boolean;
    disableModify: boolean;
};
/**
 * @callback Listener
 * @param {Event} event
 * @returns {any}
 */
/**命令类 */
declare class Command {
    #private;
    constructor(command: string, option?: CommandOption);
    /**快捷键码（只读） */
    get keyCode(): string;
    /**
     * 设置快捷键
     * @param {string} key 快捷键
     */
    bind(key: string): this;
    /**
     * 事件委托
     * @param word 选择器字符串
     */
    entrust(word: string): void;
    on(listener: Listener): this;
    /**
     * 添加侦听器
     * @param {string} 模式
     * @param {Function} listener 事件侦听器
     */
    on(mode: keyof Mode, listener: Listener): this;
    /**
     * 移除指定侦听器
     * @param {Symbol} key 键名
     */
    del(key: symbol, mode?: keyof Mode): this;
    adopt(e: Event): boolean;
    /**
     * 执行事件
     * @param {boolean} [skipCondition] 为true则跳过条件判断直接执行
     * @returns {boolean} 为true表示触发成功
     */
    trigger(event: Event, mode?: keyof Mode): boolean;
    /** * 快捷键 */
    set key(v: string);
    /** * 快捷键 */
    get key(): string;
    /**条件字符串 */
    getConditionArray(): (string | ConditionFunction)[];
    /**条件 */
    get condition(): string;
    /**
     * 添加条件
     * @param {string} condition 条件字符串
     */
    addCondition(...condition: Array<string | ConditionFunction>): this;
    /**
     * 删除条件
     * @param {string} condition 条件字符串
     */
    delCondition(...condition: string[]): this;
    /**
     * 是否存在指定条件
     * @param condition 条件字符串
     * @returns {boolean} 为true表示存在
     */
    hasCondition(condition: string): boolean;
}
