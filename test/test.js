import { flysel, Sync, sync } from '../dist/flysel.js'
const fme = flysel('#fm')
const src = {
    /**描述 */
    a: 0
}
const fm = sync(src, {
    form: fme,
    change(...ages) {
        console.log(...ages);
    }
})
window.fm = fm