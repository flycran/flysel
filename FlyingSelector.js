(() => {
   /**默认顶级对象 */
   let top = '$'
   if (document) {
      const ele = document.getElementsByTagName('script')
      const scr = ele[ele.length - 1]
      const attr = scr.getAttribute('top')
      if (attr) defaultTop = attr
   }
   (() => {
      const STYLEDEGREE = new Set([
         'width',
         'maxWidth',
         'minWidth',
         'height',
         'maxHeight',
         'minHeight',
         'left',
         'right',
         'top',
         'bottom',
         'padding',
         'margin',
         'fontSize',
         'lineHeight',
      ])
      const SEPARATOR = '#.[]:'
      /**
       * 
       * @param {HTMLElement} ele 
       * @param {string} style 
       * @param {number|string} value
       */
      function setcss(ele, style, value) {
         if (style.includes('-')) style = style.split('-').map((a, i) => i ? a[0].toUpperCase() + a.slice(1) : a).join('')
         if (STYLEDEGREE.has(style) && typeof value === 'number') value += 'px'
         ele.style[style] = value
      }
      /**HTMLElement扩展方法 */
      const HTMLElementExtend = {
         /**
         * 
         * @param {HTMLElement} ele 
         * @param {string} type 
         * @param {Function} listener 
         */
         on(type, listener, option) {
            if (typeof type === 'object') {
               for (const k in type) {
                  this.addEventListener(k, type[k], listener)
               }
               return
            }
            return this.addEventListener(type, listener, option)
         },
         /**
          * 
          * @param {string} type 
          * @param {Function} listener 
          * @this {HTMLElement}
          * @returns 
          */
         del(type, listener) {
            return this.removeEventListener(type, listener)
         },
         css(style, value) {
            if (typeof style === 'object') {
               for (const k in style) {
                  setcss(this, style, value)
               }
               return
            } else {
               setcss(this, style, value)
            }
         }
      }
      Object.assign(HTMLElement.prototype, HTMLElementExtend)
      class HTMLSet {
         constructor(eles) {
            if (eles) {
               for (let i = 0; i < eles.length; i++) {
                  this[i] = eles[i]
               }
               this.length = eles.length
            }
         }
         length = 0
         push(...e) {
            for (const v of e) {
               this[this.length] = v
            }
         }
         forEach(callBack) {
            for (let i = 0; i < this.length; i++) {

            }
         }
         get [Symbol.iterator]() {
            return Array.prototype[Symbol.iterator]
         }
      }
      /**选择器解析缓存 */
      const analysisSelectorCache = new Map
      function analysisSelector(selector) {
         const cache = analyseKeyWordCache.get(selector)
         if (cache) return cache
         const data = [];
         let word = '', sep;
         for (let i = 0; i < selector.length; i++) {
            const e = selector[i], x = SEPARATOR.indexOf(e);
            if (x === -1) {
               word += e;
            }
            else {
               if (word) {
                  const d = { word, sep };
                  if (d.sep === '[') {
                     d.word = d.word.split('=');
                  }
                  data.push(d);
                  word = '';
               }
               sep = SEPARATOR[x];
            }
         }
         const d = { word, sep };
         if (d.sep === '[') {
            d.word = d.word.split('=');
         }
         data.push(d);
         return data;
      }
      const flying = function (selector) {

      }
      Object.assign(flying, {
         HTMLSet
      })
      window[top] = flying
   })()
})()